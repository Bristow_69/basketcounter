# Basketcounter
Application HTML5 + JS pour le suivi d'un match de basket de jeunes.
On a aussi le détail des périodes pour les matches de U9 et U11.

# Binaires

- .click : version pour Ubuntu Touch
- .apk : version Android

## Note pour Android

Dans les paramétrages de sécurité, il faut accepter les sources inconnues pour l'installation.
Plus d'infos là : http://www.frandroid.com/comment-faire/lemultimedia/231266_autoriserlessourcesinconnues

# Changelog

## v0.35
- Correction d'un problème sur l'alerte JS
- Ajout du score période sur l'alerte
- Compilation du binaire Android

## v0.3
- Ajout de la gestion des périodes pour les catégories U9 et U11
- Modification du reset via un reload() (ne fonctionne pas sur Android)
- Modification de la page Aide et quelques corrections.

## v0.1
- Comptage des points
- Suivi d'un joueur
- Paramétrage des équipes


