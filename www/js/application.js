function Application(UIContext) {
    this._uiContextClass = UIContext;
    this._initialized = false;
};
Application.prototype.init = function() {
    if (this._uiContextClass && !this._initialized) {
        this._initialized = true;
        var UI = new this._uiContextClass();
        var i = 0; //permet de compter le nombre de période quand on appuie sur le bouton
        var ScoreA_Global = 0;
        var ScoreB_Global = 0;
        var ScoreA_Last = 0;	// score A de la periode précédente
        var ScoreB_Last = 0;
        UI.init();

        UI.button('3pts-A-button').click( function() {
            Fonc_ScoreA(3);
            var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastALabel.innerHTML = "+3";
            LastBLabel.innerHTML = "";
            
        });

        UI.button('3pts-B-button').click( function() {
			Fonc_ScoreB(3);
			var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastBLabel.innerHTML = "+3";
            LastALabel.innerHTML = "";
        });
			
		UI.button('2pts-A-button').click( function() {
            Fonc_ScoreA(2)
            var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastALabel.innerHTML = "+2";
            LastBLabel.innerHTML = "";
        });

        UI.button('2pts-B-button').click( function() {
			Fonc_ScoreB(2)
			var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastBLabel.innerHTML = "+2";
            LastALabel.innerHTML = "";
        });

        UI.button('1pts-A-button').click( function() {
			Fonc_ScoreA(1)
			var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastALabel.innerHTML = "+1";
            LastBLabel.innerHTML = "";
        });

        UI.button('1pts-B-button').click( function() {
			Fonc_ScoreB(1)
			var LastALabel = document.getElementById("lastA");
            var LastBLabel = document.getElementById("lastB");
            LastBLabel.innerHTML = "+1";
            LastALabel.innerHTML = "";
        });


        // reset-button quitte l'application
        UI.button('reset-button').click( function() {
            ConfirmReset();
          });
          
		UI.button('maj-equipes-button').click( function() {
            MajEquipeA();
            MajEquipeB();
          });
          
        UI.button('maj-prenom-button').click( function() {
            MajPrenomJoueur();
          });
          
        UI.button('fin-periode-button').click( function() {
            ConfirmPeriode();
          });

		function Fonc_ScoreA(point) {
            var ScoreALabel = document.getElementById('score-A');
            var ScoreA = parseInt(ScoreALabel.innerHTML);
            ScoreA = ScoreA + point;
            ScoreALabel.innerHTML = ScoreA;
            ScoreA_Global = ScoreA;
            }
            
		function Fonc_ScoreB(point) {
            var ScoreBLabel = document.getElementById('score-B');
            var ScoreB = parseInt(ScoreBLabel.innerHTML);
            ScoreB = ScoreB + point;
            ScoreBLabel.innerHTML = ScoreB;
            ScoreB_Global = ScoreB;
            }

		function MajEquipeA() {
			var NouveauNomEquipeA = document.getElementById('champ-nom-equipeA').value;
			var NomEquipeALabel = document.getElementById('nom-equipe-A');
			var NomEquipeALabelPeriodes = document.getElementById('nom-equipe-A-periodes');
			NomEquipeALabel.innerHTML = NouveauNomEquipeA;
			NomEquipeALabelPeriodes.innerHTML = NouveauNomEquipeA;

			}

		function MajEquipeB() {
			var NouveauNomEquipeB = document.getElementById('champ-nom-equipeB').value;
			var NomEquipeBLabel = document.getElementById('nom-equipe-B');
			var NomEquipeBLabelPeriodes = document.getElementById('nom-equipe-B-periodes');
			NomEquipeBLabel.innerHTML = NouveauNomEquipeB;
			NomEquipeBLabelPeriodes.innerHTML = NouveauNomEquipeB;

			}

		function MajPrenomJoueur() {
			var NouveauPrenom = document.getElementById('champ-prenom-joueur').value;
			var NouveauPrenomLabel = document.getElementById('nom-joueur-label');
			NouveauPrenomLabel.innerHTML = "&hearts; "+NouveauPrenom+" &rarr;";
			}


/* 
 * Permet de savoir qui gagne, et envoie A ou B ou AB en retour
*/
 
		function Calcul_Periode(Score_A,Score_B)
		{
				if (Score_A > Score_B)
				{
					alert("L'équipe A gagne la période : " +Score_A+" - "+Score_B);
					return('A');
				}
				else if (Score_A < Score_B)
				{
					alert("L'équipe B gagne la période : " +Score_A+" - "+Score_B);
					return('B');
				}
				else
				{
					alert("Egalité sur la période : " +Score_A+" - "+Score_B);
					return('AB');
				}
			}
	


/*

Fonctions de confirmation lorsqu'on appuie sur un bouton

*/

		function ConfirmReset()
		{
			if (confirm("On remet tout à zéro ?"))
			{
			//Reset();
			//on rafraichit la page complete, permet de tout mettre à zero
			// Fonctionne partout sauf sur tab Lenovo
			document.location.reload(true);

			}
		}

		function ConfirmPeriode()
		{
			if (confirm("Fin de la période ?"))
			{
			i = i+1;
			alert("Fin de la période " + i)
			switch (i)
			{
				case 1:
					ScoreA_P1 = ScoreA_Global;
					ScoreB_P1 = ScoreB_Global;
					var calc_1 = Calcul_Periode(ScoreA_P1,ScoreB_P1);
					document.getElementById("P1P-A").innerHTML = ScoreA_P1;
					document.getElementById("P1P-B").innerHTML = ScoreB_P1;
					if (calc_1 == 'A')
						{
						document.getElementById("P1-A").innerHTML = 1;
						document.getElementById("P1-B").innerHTML = 0;
						}
					else if (calc_1 == 'B')
						{
						document.getElementById("P1-A").innerHTML = 0;
						document.getElementById("P1-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P1-A").innerHTML = 0
						document.getElementById("P1-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA;
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;

				case 2:
					ScoreA_P2 = ScoreA_Global - ScoreA_P1;
					ScoreB_P2 = ScoreB_Global - ScoreB_P1;
					var calc_2 = Calcul_Periode(ScoreA_P2,ScoreB_P2);
					document.getElementById("P2P-A").innerHTML = ScoreA_P2;
					document.getElementById("P2P-B").innerHTML = ScoreB_P2;
					if (calc_2 == 'A')
						{
						document.getElementById("P2-A").innerHTML = 1;
						document.getElementById("P2-B").innerHTML = 0;
						}
					else if (calc_2 == 'B')
						{
						document.getElementById("P2-A").innerHTML = 0;
						document.getElementById("P2-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P2-A").innerHTML = 0;
						document.getElementById("P2-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;

				case 3:
					ScoreA_P3 = ScoreA_Global - ScoreA_P2 - ScoreA_P1;
					ScoreB_P3 = ScoreB_Global - ScoreB_P2 - ScoreB_P1;
					var calc_3 = Calcul_Periode(ScoreA_P3,ScoreB_P3);
					document.getElementById("P3P-A").innerHTML = ScoreA_P3;
					document.getElementById("P3P-B").innerHTML = ScoreB_P3;
					if (calc_3 == 'A')
						{
						document.getElementById("P3-A").innerHTML = 1;
						document.getElementById("P3-B").innerHTML = 0;
						}
					else if (calc_3 == 'B')
						{
						document.getElementById("P3-A").innerHTML = 0;
						document.getElementById("P3-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P3-A").innerHTML = 0;
						document.getElementById("P3-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;
				break;

				case 4:
					ScoreA_P4 = ScoreA_Global - ScoreA_P3 - ScoreA_P2 - ScoreA_P1;
					ScoreB_P4 = ScoreB_Global - ScoreB_P3 - ScoreB_P2 - ScoreB_P1;
					var calc_4 = Calcul_Periode(ScoreA_P4,ScoreB_P4);
					document.getElementById("P4P-A").innerHTML = ScoreA_P4;
					document.getElementById("P4P-B").innerHTML = ScoreB_P4;
					if (calc_4 == 'A')
						{
						document.getElementById("P4-A").innerHTML = 1;
						document.getElementById("P4-B").innerHTML = 0;
						}
					else if (calc_4 == 'B')
						{
						document.getElementById("P4-A").innerHTML = 0;
						document.getElementById("P4-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P4-A").innerHTML = 0;
						document.getElementById("P4-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML) + parseInt(document.getElementById("P4-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML) + parseInt(document.getElementById("P4-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;

				case 5:
					ScoreA_P5 = ScoreA_Global - ScoreA_P4 - ScoreA_P3 - ScoreA_P2 - ScoreA_P1;
					ScoreB_P5 = ScoreB_Global - ScoreB_P4 - ScoreB_P3 - ScoreB_P2 - ScoreB_P1;
					var calc_5 = Calcul_Periode(ScoreA_P5,ScoreB_P5);
					document.getElementById("P5P-A").innerHTML = ScoreA_P5;
					document.getElementById("P5P-B").innerHTML = ScoreB_P5;
					if (calc_5 == 'A')
						{
						document.getElementById("P5-A").innerHTML = 1;
						document.getElementById("P5-B").innerHTML = 0;
						}
					else if (calc_5 == 'B')
						{
						document.getElementById("P5-A").innerHTML = 0;
						document.getElementById("P5-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P5-A").innerHTML = 0;
						document.getElementById("P5-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML) + parseInt(document.getElementById("P4-A").innerHTML) + parseInt(document.getElementById("P5-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML) + parseInt(document.getElementById("P4-B").innerHTML) + parseInt(document.getElementById("P5-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;

				case 6:
					ScoreA_P6 = ScoreA_Global - ScoreA_P5 - ScoreA_P4 - ScoreA_P3 - ScoreA_P2 - ScoreA_P1;
					ScoreB_P6 = ScoreB_Global - ScoreB_P5 - ScoreB_P4 - ScoreB_P3 - ScoreB_P2 - ScoreB_P1;
					var calc_6 = Calcul_Periode(ScoreA_P6,ScoreB_P6);
					document.getElementById("P6P-A").innerHTML = ScoreA_P6;
					document.getElementById("P6P-B").innerHTML = ScoreB_P6;
					if (calc_6 == 'A')
						{
						document.getElementById("P6-A").innerHTML = 1;
						document.getElementById("P6-B").innerHTML = 0;
						}
					else if (calc_6 == 'B')
						{
						document.getElementById("P6-A").innerHTML = 0;
						document.getElementById("P6-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P6-A").innerHTML = 0;
						document.getElementById("P6-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML) + parseInt(document.getElementById("P4-A").innerHTML) + parseInt(document.getElementById("P5-A").innerHTML) + parseInt(document.getElementById("P6-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML) + parseInt(document.getElementById("P4-B").innerHTML) + parseInt(document.getElementById("P5-B").innerHTML) + parseInt(document.getElementById("P6-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;

				case 7:
					ScoreA_P7 = ScoreA_Global - ScoreA_P6 - ScoreA_P5 - ScoreA_P4 - ScoreA_P3 - ScoreA_P2 - ScoreA_P1;
					ScoreB_P7 = ScoreB_Global - ScoreB_P6 - ScoreB_P5 - ScoreB_P4 - ScoreB_P3 - ScoreB_P2 - ScoreB_P1;
					var calc_7 = Calcul_Periode(ScoreA_P7,ScoreB_P7);
					document.getElementById("P7P-A").innerHTML = ScoreA_P7;
					document.getElementById("P7P-B").innerHTML = ScoreB_P7;
					if (calc_7 == 'A')
						{
						document.getElementById("P7-A").innerHTML = 1;
						document.getElementById("P7-B").innerHTML = 0;
						}
					else if (calc_7 == 'B')
						{
						document.getElementById("P7-A").innerHTML = 0;
						document.getElementById("P7-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P7-A").innerHTML = 0;
						document.getElementById("P7-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML) + parseInt(document.getElementById("P4-A").innerHTML) + parseInt(document.getElementById("P5-A").innerHTML) + parseInt(document.getElementById("P6-A").innerHTML) + parseInt(document.getElementById("P7-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML) + parseInt(document.getElementById("P4-B").innerHTML) + parseInt(document.getElementById("P5-B").innerHTML) + parseInt(document.getElementById("P6-B").innerHTML) + parseInt(document.getElementById("P7-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;
				
				case 8:
					ScoreA_P8 = ScoreA_Global - ScoreA_P7 - ScoreA_P6 - ScoreA_P5 - ScoreA_P4 - ScoreA_P3 - ScoreA_P2 - ScoreA_P1;
					ScoreB_P8 = ScoreB_Global - ScoreB_P7 - ScoreB_P6 - ScoreB_P5 - ScoreB_P4 - ScoreB_P3 - ScoreB_P2 - ScoreB_P1;
					var calc_8 = Calcul_Periode(ScoreA_P8,ScoreB_P8);
					document.getElementById("P8P-A").innerHTML = ScoreA_P8;
					document.getElementById("P8P-B").innerHTML = ScoreB_P8;
					if (calc_8 == 'A')
						{
						document.getElementById("P8-A").innerHTML = 1;
						document.getElementById("P8-B").innerHTML = 0;
						}
					else if (calc_8 == 'B')
						{
						document.getElementById("P8-A").innerHTML = 0;
						document.getElementById("P8-B").innerHTML = 1;
						}
					else
						{
						document.getElementById("P8-A").innerHTML = 0;
						document.getElementById("P8-B").innerHTML = 0;
						}
					SommeA = parseInt(document.getElementById("P1-A").innerHTML) + parseInt(document.getElementById("P2-A").innerHTML) + parseInt(document.getElementById("P3-A").innerHTML) + parseInt(document.getElementById("P4-A").innerHTML) + parseInt(document.getElementById("P5-A").innerHTML) + parseInt(document.getElementById("P6-A").innerHTML) + parseInt(document.getElementById("P7-A").innerHTML) + parseInt(document.getElementById("P8-A").innerHTML);
					SommeB = parseInt(document.getElementById("P1-B").innerHTML) + parseInt(document.getElementById("P2-B").innerHTML) + parseInt(document.getElementById("P3-B").innerHTML) + parseInt(document.getElementById("P4-B").innerHTML) + parseInt(document.getElementById("P5-B").innerHTML) + parseInt(document.getElementById("P6-B").innerHTML) + parseInt(document.getElementById("P7-B").innerHTML) + parseInt(document.getElementById("P8-B").innerHTML);
					document.getElementById("Somme-A").innerHTML = SommeA
					document.getElementById("Somme-B").innerHTML = SommeB;

				break;
			}

			}
		}

	}
};

Application.prototype.initialized = function() {
    return this._initialized;
};

